//Solidity backend for EthMesh
//Copyright Christopher Mackinga 2017


contract mortal{

	address public owner;

	function mortal(){
		owner = msg.sender;
	}

	modifier onlyOwner{
		if (msg.sender != owner){
			throw;
		}else{
			_
		}
	}

	function kill() onlyOwner{
		suicide(owner);
	}
}


contract User is mortal{

	string public userName;
	uint postNum;

	mapping(uint=>Post) public posts;

	struct Post {
		bool exists;
		unit postNum;
		string bodyText;
		uint lastUpdate;

		/*
		uint likeNum
		mapping(address=>Like) public likes;

		uint commentNum;
		mapping(uint=>Comment) public comments;
		/*
		
	}
	
	/*
	struct Comment {
		bool exists;
		string	commentText;
		uint	lastUpdated;
		uint	likeNum;
		mapping(address=>Like) public likes;
	}

	struct Like {
		bool doesLike;
		uint lastUpdate;
	}
	*/

	function User(string _name){
		userName = _name;
		postNum = 0;
	}

	function makePost(string _bodyText) onlyOwner {
		posts[postNum + 1] = Post({
			postNum: postNum + 1,
			lastUpdate: now,
			bodyText: _bodyText,
//			likenum: 0,
//			commentNum: 0,
			exists: true 
			});
		postNum = postNum + 1;
	}


	function editPost(uint _postNum, string _bodyText) onlyOwner{
		if(posts[_postNum].exists){
			posts[_postNum].bodyText = _bodyText;
			posts[_postNum.lastUpdate = now;
		} else {
			throw;
		}
	}

	function deletePost(uint _postNum) onlyOwner{
		if(posts[_postNum].exists){
			posts[_postNum].exists = false;
			posts[_postNum].bodyText = "";
		}else{
			throw;
		}
	}


}

